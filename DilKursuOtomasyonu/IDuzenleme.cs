﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DilKursuOtomasyonu
{
    interface IDuzenleme
    {
        //deneme bu
        void Ekle(BaseUser user, Kullanici k);
        void Sil(BaseUser user, Kullanici k);
        void Guncelle(BaseUser user, Kullanici k);
    }
}
