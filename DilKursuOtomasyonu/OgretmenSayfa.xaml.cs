﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DilKursuOtomasyonu
{
    /// <summary>
    /// OgretmenSayfa.xaml etkileşim mantığı
    /// </summary>
    public partial class OgretmenSayfa : Window
    {

        public int KId {get; set;}
        public OgretmenSayfa()
        {
            InitializeComponent();
        }
        DbDilKursuOtomsyonuEntities db = new DbDilKursuOtomsyonuEntities();

        private void ogrtTab_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dpogItem.IsSelected)
            {
                var sorgu = from ogrt in db.Ogretmen
                            where ogrt.id == KId
                            join ders in db.Ders on ogrt.id equals ders.ogrtId
                            join prog in db.DersProg on ders.dersProgId equals prog.id
                            join dil in db.Dil on ders.dilId equals dil.id
                            join kur in db.Kur on ders.kurId equals kur.id
                            select new
                            {
                                ders.gun,
                                ders.saat,
                                dil.ad,
                                kur.seviye,
                                ders.derslik
                            };
                dtgridDersPorg.ItemsSource = sorgu.ToList();
            }
            if (ayarlarItem.IsSelected)
            {

                    // ayarlar kısmını açtığımız anda ad ve soyadın karşımıza gelip değişiklik yapılamaması için
                    var sor = (from v in db.Ogretmen
                               where v.kullaniciId == KId
                               select v).FirstOrDefault();
                    txt_ad.Text = sor.ad;
                    txt_soyad.Text = sor.soyad;

                    txt_ad.IsEnabled = false;
                    txt_soyad.IsEnabled = false;

             }


        }
        private void txt_tel_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!char.IsDigit(e.Text, e.Text.Length - 1))
            {
                e.Handled = true;
            }
        }

        private void btn_ogrenci_ayar_Click(object sender, RoutedEventArgs e)
        {
        
                // Kullanıcı ayarları kısmında elemanın bilgilerini günceller
                var guncellenecek = (from v in db.Ogrenci
                                     where v.kullaniciId == KId
                                     select v).FirstOrDefault();
                guncellenecek.email = txt_mail.Text;
                guncellenecek.tel = txt_tel.Text;
                guncellenecek.Kullanici.sifre = txt_sifre.Text;
                db.SaveChanges();
                MessageBox.Show("Güncellendi !");
        }
    }
}
