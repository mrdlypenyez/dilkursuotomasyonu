﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace DilKursuOtomasyonu
{
    /// <summary>
    /// ElemanSayfasi.xaml etkileşim mantığı
    /// </summary>
    public partial class ElemanSayfasi : Window
    {
        public int KId { get; set; }


        public ElemanSayfasi()
        {
            InitializeComponent();
        }
        Duzenleme islem = new Duzenleme();

        private void TabControl_SelectionChanged(object sender, RoutedEventArgs e)
        {

            if (kurItem.IsSelected)
            {
            }
        }


            String kadi;
        private void btnara_Click(object sender, RoutedEventArgs e)
        {

           kadi = txttc.Text;
           

            using (DbDilKursuOtomsyonuEntities db = new DbDilKursuOtomsyonuEntities())
            {
                Kullanici k = db.Kullanici.FirstOrDefault(a => a.k_adi == kadi);
                
                var al = from ogr in db.Ogrenci
                         where ogr.kullaniciId==k.id
                         join odeme in db.Odeme on ogr.id equals odeme.ogrid
                         where odeme.durum == false
                         join ders in db.Ders on odeme.dersId equals ders.id
                         select new
                         {
                             ogrId = ogr.id,
                             ogr.ad,
                             ogr.soyad,
                             ders.fiyat,
                             odeme.durum

                         };

               if (al.Any())
                {
                    chcboxOdendi.IsEnabled = true;
                    lstbxBilgi.ItemsSource = al.ToList();
                }
                else
                {
                    chcboxOdendi.IsEnabled = false;
                    MessageBox.Show("Öğrencinin kayıtlı borcu bulunmamkatadır.");
                    sifirla();
                }

            }

        }


        private void chcboxOdendi_Checked(object sender, RoutedEventArgs e)
        {
            using (DbDilKursuOtomsyonuEntities db = new DbDilKursuOtomsyonuEntities())
            {
                int a = 0;
                Kullanici k = db.Kullanici.FirstOrDefault(y => y.k_adi == kadi);
                Ogrenci ogr = db.Ogrenci.FirstOrDefault(x => x.kullaniciId == k.id);
                
                foreach (Odeme o in db.Odeme)
                {
                    if (o.ogrid == ogr.id)
                    {
                        o.durum = true;
                        o.tarih = DateTime.Now;
                        a = 1;
                        break;
                    }
                }
                if (a == 1)
                {
                    db.SaveChanges();
                    MessageBox.Show("Borç ödendi.");
                    sifirla();
                }
                else
                {
                    MessageBox.Show("derbeder.");
                    sifirla();
                }
            }
            //ogrid = 0;
        }
        public void sifirla()
        {
            chcboxOdendi.IsChecked = false;
            lstbxBilgi.ItemsSource = "";
            txttc.Text = "";
            txtkadi.Text = "";
            txtad.Text = "";
            txtsoyad.Text = "";
            txttel.Text = "";
            txtemail.Text = "";

        }

        private void btnEkle_Click(object sender, RoutedEventArgs e)
        {
            

            Kullanici k = new Kullanici();
            Ogrenci o = new Ogrenci();
            o.ad = txtad.Text;
            o.soyad = txtsoyad.Text;
            o.tel = txttel.Text;
            o.email = txtemail.Text;
            o.dersProgId = 4;

            k.k_adi = txtkadi.Text;
            k.yetkiId = 4;
            k.sifre = k.k_adi.Substring(0, 2) + o.ad.Substring(0, 3);

            islem.Ekle(o, k);//öğrenci eklendi

            sifirla();

        }

        private void SbtnAra_Click(object sender, RoutedEventArgs e)
        {
            Ogrenci og;
            String kadi = txttcS.Text;

                using (DbDilKursuOtomsyonuEntities db = new DbDilKursuOtomsyonuEntities())
                {
                Kullanici k = db.Kullanici.FirstOrDefault(y => y.k_adi == kadi);
                og = db.Ogrenci.FirstOrDefault(x => x.kullaniciId == k.id);
                   
                    var sorgu = from ogr in db.Ogrenci
                                where (ogr.id == og.id)
                                join o in db.Odeme on ogr.id equals o.ogrid
                                where o.durum==true
                                join ders in db.Ders on o.dersId equals ders.id
                                select new
                                {
                                    ogr.ad,
                                    ogr.soyad,
                                    ders.fiyat,
                                    o.durum

                                    };
                    if (sorgu.Any())
                    {
                        dtgogrGoster.ItemsSource = sorgu.ToList();
                    }
                    else
                    {
                        MessageBox.Show("Uygun Şartı Sağlayan Öğrenci Bulunmadı.Taksitlerini Kontrol ediniz");

                        taksit.IsSelected = true;
                    }
                }
          



        }

        private void Btnsil_Click(object sender, RoutedEventArgs e)
        {
           String kadi=txttcS.Text;
            Kullanici kullanici;
            Ogrenci ogrenci;
            using (DbDilKursuOtomsyonuEntities db=new DbDilKursuOtomsyonuEntities())
            {
                kullanici = db.Kullanici.FirstOrDefault(x => x.k_adi==kadi);
                ogrenci = db.Ogrenci.FirstOrDefault(y => y.kullaniciId==kullanici.id);
                 db.Ogrenci.Remove(ogrenci);
                 db.Kullanici.Remove(kullanici);

                 try
                 {
                     db.SaveChanges();
                     MessageBox.Show("Öğrenci Başarıyla Silindi.");

                 }
                 catch (Exception ex)
                 {
                     MessageBox.Show("Sistemsel arıza oluştu.Sonradan tekrar deneyin." + ex.InnerException.Message);
                 }
              
            }
        }
        string id;
        private void kurDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            // 6 index numaralı sütuna dersprodId yi yerleştirdik ama datagrid de gizli 
            id = ((TextBlock)kurDataGrid.Columns[5].GetCellContent(kurDataGrid.SelectedItem)).Text;
            MessageBox.Show("Seçildi");
            btn_kura_ekle.IsEnabled = true;

        }
        private void btn_kura_ekle_Click(object sender, RoutedEventArgs e)
        {

            using (DbDilKursuOtomsyonuEntities db = new DbDilKursuOtomsyonuEntities())
            {
                string tc = txt_kura_ekle.Text;
                var guncel = (from o in db.Ogrenci
                              where
                              o.Kullanici.k_adi == tc
                              select o).FirstOrDefault();

                guncel.dersProgId = Convert.ToInt32(id);
                db.SaveChanges();

                MessageBox.Show(guncel.DersProg.ad + " " + "Kura Eklendi");
                txt_kura_ekle.Text = "";
            }

        }


        private void kurItem_Loaded(object sender, RoutedEventArgs e)
        {
            using (DbDilKursuOtomsyonuEntities db = new DbDilKursuOtomsyonuEntities())
            {
                // Öncelikle istediğimiz kuru datagritten seçip daha sonra öğrencimizin tc sini gireceğiz.

                var ara = from o in db.Kur
                          join ders in db.Ders on o.id equals ders.kurId
                          join dprog in db.DersProg on ders.dersProgId equals dprog.id
                          join ogrt in db.Ogretmen on ders.ogrtId equals ogrt.id
                          select new
                          {
                              o.seviye,
                              ders.gun,
                              ders.saat,
                              ogrtad = ogrt.ad + " " + ogrt.soyad,
                              ders.derslik,
                              dprog.id,
                             
                          };

                kurDataGrid.ItemsSource = ara.ToList();
                btn_kura_ekle.IsEnabled = false;
            }
        }
        // telefon numarasını sadece rakam olarak alır
        private void txt_tel_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!char.IsDigit(e.Text, e.Text.Length - 1))
            {
                e.Handled = true;
            }
        }

        private void btn_ayar_guncelle_Click(object sender, RoutedEventArgs e)
        {
            using (DbDilKursuOtomsyonuEntities db = new DbDilKursuOtomsyonuEntities())
            {
               
                // Kullanıcı ayarları kısmında elemanın bilgilerini günceller
                var guncellenecek = (from v in db.Eleman
                                     where v.kullaniciId == KId
                                     select v).FirstOrDefault();
               
                
               guncellenecek.email = txt_mail.Text;
                guncellenecek.tel = txt_tel.Text;
                guncellenecek.Kullanici.sifre = txt_sifre.Text;
                db.SaveChanges();
                MessageBox.Show("Güncellendi !");

            }
        }

        private void k_ayarlari_Loaded(object sender, RoutedEventArgs e)
        {
            using (DbDilKursuOtomsyonuEntities db = new DbDilKursuOtomsyonuEntities())
            {
                var k = (from kullanici in db.Kullanici
                         where kullanici.id == KId
                         join eleman in db.Eleman on kullanici.id equals eleman.kullaniciId
                         select eleman).FirstOrDefault();
                txt_ad.Text = k.ad;
                txt_soyad.Text = k.soyad;
                txt_ad.IsReadOnly = true;
                txt_soyad.IsReadOnly = true;
            }

        }
    }
}
