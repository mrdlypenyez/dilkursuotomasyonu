﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DilKursuOtomasyonu
{
    /// <summary>
    /// OgrenciSayfasi.xaml etkileşim mantığı
    /// </summary>
    public partial class OgrenciSayfasi : Window
    {
        public int KId { get; set; }
     
        public OgrenciSayfasi()
        {
            InitializeComponent();
        }
        public DbDilKursuOtomsyonuEntities db = new DbDilKursuOtomsyonuEntities();
        private void OgrTab_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (dpogItem.IsSelected)
            {
                ////ders prog tıklandığında oluşacak olan olaylar
                var sorgu = from o in db.Ogrenci
                            where
                            o.kullaniciId == KId
                            join dprog in db.DersProg on o.dersProgId equals dprog.id
                            join ders in db.Ders on dprog.id equals ders.dersProgId
                            join kr in db.Kur on ders.kurId equals kr.id
                            join dil in db.Dil on ders.dilId equals dil.id
                            select new
                            {
                                ders.gun,
                                ders.saat,
                                dil.ad,
                                kr.seviye,
                                ders.derslik
                            };

                dtgridDersPorg.ItemsSource = sorgu.ToList();
                
            }
            if (odemeItem.IsSelected)
            {
                if (arsivExpander.IsExpanded)
                {
                    var sorgu = from o in db.Ogrenci
                                where o.kullaniciId == KId
                                join odeme in db.Odeme on o.id equals odeme.ogrid
                                join ders in db.Ders on odeme.dersId equals ders.id
                                join kr in db.Kur on ders.kurId equals kr.id
                                join dil in db.Dil on ders.dilId equals dil.id
                                where odeme.durum == true
                                select new
                                {
                                    dil.ad,
                                    kr.seviye,
                                    odeme.tarih,
                                    ders.fiyat

                                };

                    arsivGrid.ItemsSource = sorgu.ToList();
                   // this.

                }
                if (odenecekExpander.IsExpanded)
                {
                    var sorgu= from o in db.Ogrenci
                                 where o.kullaniciId == KId
                                join odeme in db.Odeme on o.id equals odeme.ogrid
                                join ders in db.Ders on odeme.dersId equals ders.id
                                join kr in db.Kur on ders.kurId equals kr.id
                                join dil in db.Dil on ders.dilId equals dil.id
                                where odeme.durum == false
                                select new
                                {
                                    dil.ad,
                                    kr.seviye,
                                    odeme.tarih,
                                    ders.fiyat

                                };
                    odemeGrid.ItemsSource = sorgu.ToList();
                }
               
                
            }
            if (ayarlarItem.IsSelected)
            {
                //ayarlara tıklandığında oluşacak olan olaylar fakat burda bir !! interface kullananımı olacak
            }

        }
        private void btn_ayar_guncelle_Click(object sender, RoutedEventArgs e)
        {
            using (DbDilKursuOtomsyonuEntities db = new DbDilKursuOtomsyonuEntities())
            {

                // Kullanıcı ayarları kısmında elemanın bilgilerini günceller
                var guncellenecek = (from v in db.Eleman
                                     where v.kullaniciId == KId
                                     select v).FirstOrDefault();


                guncellenecek.email = txt_mail.Text;
                guncellenecek.tel = txt_tel.Text;
                guncellenecek.Kullanici.sifre = txt_sifre.Text;
                db.SaveChanges();
                MessageBox.Show("Güncellendi !");

            }
        }

        private void k_ayarlari_Loaded(object sender, RoutedEventArgs e)
        {
            using (DbDilKursuOtomsyonuEntities db = new DbDilKursuOtomsyonuEntities())
            {
                var k = (from kullanici in db.Kullanici
                         where kullanici.id == KId
                         join ogr in db.Ogrenci on kullanici.id equals ogr.kullaniciId
                         select ogr).FirstOrDefault();
                txt_ad.Text = k.ad;
                txt_soyad.Text = k.soyad;
                txt_ad.IsReadOnly = true;
                txt_soyad.IsReadOnly = true;
            }

        }


    }
}

