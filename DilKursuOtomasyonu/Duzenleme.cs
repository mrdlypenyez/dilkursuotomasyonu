﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
// deneme git
//deneme 2
namespace DilKursuOtomasyonu
{
    class Duzenleme
    {
        public void Ekle(BaseUser user, Kullanici k)
        {
            using (DbDilKursuOtomsyonuEntities db = new DbDilKursuOtomsyonuEntities())
            {

                if (user.GetType() == typeof(Ogrenci))
                {
                    db.Kullanici.Add(k);
                    k.Ogrenci.Add((Ogrenci)user);

                }
                if (user.GetType() == typeof(Ogretmen))
                {
                    db.Kullanici.Add(k);
                    k.Ogretmen.Add((Ogretmen)user);
                }
                if (user.GetType() == typeof(Eleman))
                {
                    db.Kullanici.Add(k);
                    k.Eleman.Add((Eleman)user);
                }

                try
                {
                    db.Configuration.LazyLoadingEnabled = false;
                    db.SaveChanges();
                    MessageBox.Show(" Başarıyla Kaydedildi.");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Kayıt yapılmadı.Sistemsel hata oluştu.Daha sonra tekrar deneyiniz. " + ex.InnerException.Message);
                }
            }
        }


        public void Guncelle(BaseUser user, Kullanici k) // Kullanıcı ayarları kısımlarında çağrılacak,iki kere aramaya yapmak zorunda kalacak verimliliğinden emin değilim 
        {
            using(DbDilKursuOtomsyonuEntities db=new DbDilKursuOtomsyonuEntities())
            {
                if (user.GetType() == typeof(Ogrenci)) //Öğrenci güncellenmek istenirse
                {


                }
                else if (user.GetType() == typeof(Ogretmen))
                {

                }else if (user.GetType() == typeof(Eleman))
                {

                }


            }


        }
       
        public void Sil(BaseUser user,Kullanici kullanici)
        {
            
            using(DbDilKursuOtomsyonuEntities db=new DbDilKursuOtomsyonuEntities())//sil kodu.
            {
                if (user.GetType() == typeof(Ogrenci))
                {
                   // Ogrenci ogrenci = db.Ogrenci.FirstOrDefault(x => x.kullaniciId==kullanici.id);
                   

                    // db.Ogrenci.Remove((Ogrenci)user);
                    kullanici.Ogrenci.Remove((Ogrenci)user);
                    db.Kullanici.Remove((Kullanici)kullanici);

                    try
                    {
                        //db.Configuration.LazyLoadingEnabled = false;
                        db.SaveChanges();
                        MessageBox.Show("Öğrenci Başarıyla Silindi.");

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Sistemsel arıza oluştu.Sonradan tekrar deneyin." + ex.InnerException.Message);
                    }

                }
               

            }
           
              

        }
       
        
    }
}
