﻿#pragma checksum "..\..\OgrenciSayfasi.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "A60F843DBAE59155E7A2606E54D6C431FC91272665F675CF51D4C88C0F85089C"
//------------------------------------------------------------------------------
// <auto-generated>
//     Bu kod araç tarafından oluşturuldu.
//     Çalışma Zamanı Sürümü:4.0.30319.42000
//
//     Bu dosyada yapılacak değişiklikler yanlış davranışa neden olabilir ve
//     kod yeniden oluşturulursa kaybolur.
// </auto-generated>
//------------------------------------------------------------------------------

using DilKursuOtomasyonu;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace DilKursuOtomasyonu {
    
    
    /// <summary>
    /// OgrenciSayfasi
    /// </summary>
    public partial class OgrenciSayfasi : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\OgrenciSayfasi.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabControl OgrTab;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\OgrenciSayfasi.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabItem dpogItem;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\OgrenciSayfasi.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dtgridDersPorg;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\OgrenciSayfasi.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabItem odemeItem;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\OgrenciSayfasi.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Expander arsivExpander;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\OgrenciSayfasi.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid arsivGrid;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\OgrenciSayfasi.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Expander odenecekExpander;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\OgrenciSayfasi.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid odemeGrid;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\OgrenciSayfasi.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabItem ayarlarItem;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/DilKursuOtomasyonu;component/ogrencisayfasi.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\OgrenciSayfasi.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.OgrTab = ((System.Windows.Controls.TabControl)(target));
            
            #line 10 "..\..\OgrenciSayfasi.xaml"
            this.OgrTab.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.OgrTab_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 2:
            this.dpogItem = ((System.Windows.Controls.TabItem)(target));
            return;
            case 3:
            this.dtgridDersPorg = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 4:
            this.odemeItem = ((System.Windows.Controls.TabItem)(target));
            return;
            case 5:
            this.arsivExpander = ((System.Windows.Controls.Expander)(target));
            return;
            case 6:
            this.arsivGrid = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 7:
            this.odenecekExpander = ((System.Windows.Controls.Expander)(target));
            return;
            case 8:
            this.odemeGrid = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 9:
            this.ayarlarItem = ((System.Windows.Controls.TabItem)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

