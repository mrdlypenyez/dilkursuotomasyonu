﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DilKursuOtomasyonu
{

    /// <summary>
    /// Login.xaml etkileşim mantığı
    /// </summary>
    

    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
        }

        public Kullanici kullanici;
       
        public void Button_Click(object sender, RoutedEventArgs e)
        {
            int kontrol = 0;
            string kadi = txttc.Text, sifre = pswsifre.Password;
            Kullanici kullanici = new Kullanici();
            using (DbDilKursuOtomsyonuEntities db = new DbDilKursuOtomsyonuEntities())
            {
                foreach (Kullanici i in db.Kullanici)
                {
                    if (string.Compare(kadi, i.k_adi) == 0)
                    {
                        if (string.Compare(sifre, i.sifre) == 0)
                        {
                            kullanici = i;
                            kontrol = 1;
                            break;
                        }

                    }
                }
            }
            if (kontrol == 1)
            {
                switch (Convert.ToInt32(kullanici.yetkiId))
                {
                    case 1:
                        YoneticiSayfasi yoneticiSayfasi = new YoneticiSayfasi();
                        yoneticiSayfasi.Show();
                        this.Close();
                        break;

                    case 2:
                       
                        ElemanSayfasi elemanSayfa = new ElemanSayfasi();
                        elemanSayfa.KId = kullanici.id;
                        elemanSayfa.Show();
                        this.Close();
                        break;


                    case 3:
                        OgretmenSayfa ogretmenSayfa = new OgretmenSayfa();
                        ogretmenSayfa.KId = kullanici.id;
                        ogretmenSayfa.Show();
                        break;
                    case 4:
                        OgrenciSayfasi ogrencisayfasi = new OgrenciSayfasi();
                        ogrencisayfasi.KId = kullanici.id;
                        ogrencisayfasi.Show();
                        break;
                   


                }

            }
            else
            {
                MessageBox.Show("Geçersiz şifre ya da tc");

                txttc.Text = "";
                pswsifre.Password = "";
            }
        }

        
    }

}
