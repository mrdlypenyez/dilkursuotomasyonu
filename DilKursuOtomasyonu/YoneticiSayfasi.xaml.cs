﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MessageBox = System.Windows.MessageBox;

namespace DilKursuOtomasyonu
{
    /// <summary>
    /// YoneticiSayfasi.xaml etkileşim mantığı
    /// </summary>
    
    
    public partial class YoneticiSayfasi : Window
    {
        public int KId { get; set; }
        public YoneticiSayfasi()
        {
            InitializeComponent();
        }
        Duzenleme AdminIslem = new Duzenleme();
        private void listeleitem_Loaded(object sender, RoutedEventArgs e)
        {
            using (DbDilKursuOtomsyonuEntities db = new DbDilKursuOtomsyonuEntities())
            {
               // dt_grid_ogr.IsReadOnly = true;
                var sorgu = from o in db.Ogrenci
                            join ders in db.Ders on o.dersProgId equals ders.id
                            join kur in db.Kur on ders.kurId equals kur.id
                            join ogrt in db.Ogretmen on ders.ogrtId equals ogrt.id
                            select new
                            {
                                o.ad,
                                o.soyad,
                                kur.seviye,
                                ogrtad=ogrt.ad+" "+ogrt.soyad
                            };
                dt_grid_ogr.ItemsSource = sorgu.ToList();
            }

        }

        private void BtnOgrEkle_Click(object sender, RoutedEventArgs e)
        {
            Ogretmen ogretmen = new Ogretmen();
            Kullanici kullanici = new Kullanici();
            Dil  secilen;

            kullanici.k_adi = txtkadiOgr.Text;
            ogretmen.ad = txtadOgr.Text;
            ogretmen.soyad = txtsoyadOgr.Text;
            ogretmen.tel = txttelOgr.Text;
            ogretmen.email = txtemailOgr.Text;
            ogretmen.btarihi = DateTime.Now;
            secilen = (Dil)cmbDil.SelectedItem;

            kullanici.sifre = kullanici.k_adi.Substring(0, 3) + ogretmen.ad.Substring(0, 2);
            kullanici.yetkiId = 3;
           // ogretmen.resim = new BitmapImage(new Uri(resim));
            using (DbDilKursuOtomsyonuEntities db = new DbDilKursuOtomsyonuEntities())
            {
                secilen = db.Dil.FirstOrDefault(d => d.id == secilen.id);
                ogretmen.dilId =secilen.id;
            }
            AdminIslem.Ekle(ogretmen, kullanici);

        }
        
        private void TabControl_Loaded(object sender, RoutedEventArgs e)
        {
            using(DbDilKursuOtomsyonuEntities db=new DbDilKursuOtomsyonuEntities())
            {
                cmbDil.ItemsSource = db.Dil.ToList();
                // ayarlar kısmını açtığımız anda ad ve soyadın karşımıza gelip değişiklik yapılamaması içi
            }
            
        }
        String resim;
        private void BtnresimSec_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dosya = new OpenFileDialog();
            dosya.Filter = "Resim Dosyası | *.jpg; *.png";
            dosya.CheckFileExists = true; //dosya kontrol
            dosya.Multiselect = false;
            dosya.ShowDialog();
            resim = dosya.FileName;
        }

        string kadi;
        private void SbtnAra_Click(object sender, RoutedEventArgs e)
        {
            kadi = txttcS.Text;
            

            using (DbDilKursuOtomsyonuEntities db=new DbDilKursuOtomsyonuEntities())
            {
                var getir = from k in db.Kullanici
                            where kadi == k.k_adi
                            join ogrt in db.Ogretmen
                            on k.id equals ogrt.kullaniciId
                            join dil in db.Dil
                            on ogrt.dilId equals dil.id
                            select new
                            {
                                ogrt.ad,
                                ogrt.soyad,
                                dila=dil.ad,
                                ogrt.btarihi

                            };
                if (getir.Any())
                {
                    dtgogrtGoster.ItemsSource = getir.ToList();
                }
                else
                {
                    MessageBox.Show("Aradığınız kullanıcı bulunammamıştır.Tekrar Deneyiniz.");
                    txttcS.Text = "";
                }
            }
           
        }

        public void Btnsil_Click(object sender, RoutedEventArgs e)
        {
            
            Kullanici kullanici;
            Ogretmen ogretmen;
            using(DbDilKursuOtomsyonuEntities db=new DbDilKursuOtomsyonuEntities())
            {
                kullanici = db.Kullanici.FirstOrDefault(x => x.k_adi == kadi);
                ogretmen = db.Ogretmen.FirstOrDefault(y => y.kullaniciId == kullanici.id);

                db.Kullanici.Remove(kullanici);
                db.Ogretmen.Remove(ogretmen);

                try
                {
                    db.SaveChanges();
                    MessageBox.Show("Kullanıcı Başarıyla Silindi.");
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Sistemsel arıza oluştu.Sonradan tekrar deneyin." + ex.InnerException.Message);
                }
            }
        }

        private void BtnElemanEkle_Click(object sender, RoutedEventArgs e)
        {
            Kullanici k = new Kullanici();
            Eleman eleman = new Eleman();
            eleman.ad = txtad.Text;
            eleman.soyad = txtsoyad.Text;
            eleman.tel = txttel.Text;
            eleman.email = txtemail.Text;
            

            k.k_adi = txtkadi.Text;
            k.yetkiId = 2;
            k.sifre = k.k_adi.Substring(0, 2) + eleman.ad.Substring(0, 3);
            AdminIslem.Ekle(eleman,k);
            
            sifirla();
        }
        public void sifirla()
        {
            txtad.Text = "";
            txtsoyad.Text = "";
            txttel.Text = "";
            txtemail.Text = "";
            txttcS.Text = "";
            txtkadi.Text = "";
            txttcSe.Text = "";
            dtgogrtGoster.ItemsSource = "";
            dtgElemanGoster.ItemsSource = "";
        }

        private void BtnElemansil_Click(object sender, RoutedEventArgs e)
        {
            Kullanici kullanici;
            Eleman eleman;
            using(DbDilKursuOtomsyonuEntities db=new DbDilKursuOtomsyonuEntities())
            {
                kullanici = db.Kullanici.FirstOrDefault(x => x.k_adi == ekadi);
                eleman = db.Eleman.FirstOrDefault(y => y.kullaniciId == kullanici.id);
                db.Kullanici.Remove(kullanici);
                db.Eleman.Remove(eleman);
                try
                {
                    db.Configuration.LazyLoadingEnabled = false;
                    db.SaveChanges();
                    MessageBox.Show("Başarıyla Silindi.");
                    sifirla();
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Sistemsel Hata oluştu.Tekrar Deneyiniz" + ex.InnerException.Message);
                }
            }

        }
        string ekadi;
        private void BtnAraEleman_Click(object sender, RoutedEventArgs e)
        {
            ekadi = txttcSe.Text;
            using (DbDilKursuOtomsyonuEntities db = new DbDilKursuOtomsyonuEntities())
            {
                var ara = from k in db.Kullanici
                          where k.k_adi == ekadi
                          join eleman in db.Eleman
                          on k.id equals eleman.kullaniciId
                          select new
                          {
                              eleman.ad,
                              eleman.soyad,
                              eleman.tel
                          };
                dtgElemanGoster.ItemsSource = ara.ToList();
            }

        }
        //Ders Ekle Kısımları
        Kur kur;
        string gun;
        string derslik;
        private void BtnKontrol_Click(object sender, RoutedEventArgs e)
        {
             kur = (Kur)cmbKur.SelectedItem;
            derslik = (string)cmbDerslik.SelectedItem;
            // ogretmen =(string)cmbOgrt.SelectedItem;
             gun = (string)cmbGun.SelectedItem;

            using (DbDilKursuOtomsyonuEntities db = new DbDilKursuOtomsyonuEntities())
            {
                var sorgu = from ders in db.Ders
                            where (ders.dilId == secilenDil.id &secilenOgretmen.id==ders.ogrtId
                            & ders.kurId == kur.id & ders.saat == txtsaat.Text & ders.gun == gun)
                            select ders;
                if (sorgu.Any())
                {
                    MessageBox.Show("Kaydetmeye çalıştığınız ders tanımlıdır.Farklı Parametrelerle tekrar deneyiniz. ");
                }
                else
                {
                    MessageBox.Show("Ders Kaydetmeye Uygundur.Onaylatabilirsiniz");
                    BtnKontrol.IsEnabled = false;
                    BtnOnay.IsEnabled = true;
                }

            }
        }

        private void BtnOnay_Click(object sender, RoutedEventArgs e)
        {
            Ders ders = new Ders();
            DersProg prog = new DersProg();

            ders.dilId = secilenDil.id;
            ders.kurId = kur.id;
            ders.fiyat = secilenDil.id * 250 + kur.id * 250;
            ders.gun = gun;
            ders.saat = txtsaat.Text;
            ders.ogrtId = secilenOgretmen.id;
            ders.derslik = derslik;
            prog.ad = "Prog_Y";
                
            using (DbDilKursuOtomsyonuEntities db = new DbDilKursuOtomsyonuEntities())
            {
                prog.Ders.Add(ders);
                db.DersProg.Add(prog);
                try
                {
                    db.Configuration.LazyLoadingEnabled = false;
                    db.SaveChanges();
                    MessageBox.Show("Ders Başarıyla Oluşturuldu.");
                    sifirlaCmb();
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Ders Eklenemedi.Tekrar Deneyiniz." + ex.ToString());
                }
            }
            
        }

        Dil secilenDil;
        Ogretmen secilenOgretmen;
        public void dersolusturitem_Loaded(object sender, RoutedEventArgs e)
        {
            using (DbDilKursuOtomsyonuEntities db = new DbDilKursuOtomsyonuEntities())
            {
                
                cmbDild.ItemsSource = db.Dil.ToList();
                cmbDild.SelectedIndex = 0;
                
                cmbKur.ItemsSource = db.Kur.ToList();
                
            }
            cmbGun.Items.Add("Pazartesi");
            cmbGun.Items.Add("Salı");
            cmbGun.Items.Add("Çarşamba");
            cmbGun.Items.Add("Perşembe");
            cmbGun.Items.Add("Cuma");
            BtnOnay.IsEnabled = false;
            cmbDerslik.Items.Add("A001");
            cmbDerslik.Items.Add("A002");
            cmbDerslik.Items.Add("B001");
            cmbDerslik.Items.Add("B002");
            cmbDerslik.Items.Add("C000");
        }

        private void cmbDild_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            secilenDil = (Dil)cmbDild.SelectedItem;
            using(DbDilKursuOtomsyonuEntities db=new DbDilKursuOtomsyonuEntities())
            {
                secilenOgretmen = db.Ogretmen.FirstOrDefault(a => a.dilId == secilenDil.id);
                var sorgu = from ogrt in db.Ogretmen
                            where secilenDil.id == ogrt.dilId
                            select new { ogrt.ad, ogrt.soyad };
                cmbOgrt.ItemsSource = sorgu.ToList();

            }
        }
        public void sifirlaCmb()
        {
            dersolusturitem.IsSelected = true;
        }
        

        private void dpolusturitem_Loaded(object sender, RoutedEventArgs e)
        {
            using (DbDilKursuOtomsyonuEntities db = new DbDilKursuOtomsyonuEntities())
            {
                var getir = from ders in db.Ders
                            join dil in db.Dil on ders.dilId equals dil.id
                            join kur in db.Kur on ders.kurId equals kur.id
                            join ogrt in db.Ogretmen on ders.ogrtId equals ogrt.id
                            select new
                            {
                                ders.id,
                                ders.gun,
                                ders.saat,
                                dilad = dil.ad,
                                kur.seviye,
                                ogrtad = ogrt.ad + " " + ogrt.soyad,
                                ders.derslik

                            };
                GrddersGoster.ItemsSource = getir.ToList();
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            string aranan = txtDilAra.Text;
            using (DbDilKursuOtomsyonuEntities db = new DbDilKursuOtomsyonuEntities())
            {
                var yenile = from ders in db.Ders
                             join dil in db.Dil on ders.dilId equals dil.id     
                             join kur in db.Kur on ders.kurId equals kur.id
                             join ogrt in db.Ogretmen on ders.ogrtId equals ogrt.id
                             where (dil.ad.Contains(aranan) |  kur.seviye.Contains(aranan) | ders.gun.Contains(aranan) | ogrt.ad.Contains(aranan))
                             select new
                             {
                                 ders.id,
                                 ders.gun,
                                 ders.saat,
                                 dilad = dil.ad,
                                 kur.seviye,
                                 ogrtad = ogrt.ad + " " + ogrt.soyad,
                                 ders.derslik

                             };
                GrddersGoster.ItemsSource = yenile.ToList();
            }
        }

        public object Doldur(int id)
        {
            using (DbDilKursuOtomsyonuEntities db = new DbDilKursuOtomsyonuEntities())
            {
                var x = from ders in db.Ders
                        where ders.id == id
                        join dil in db.Dil on ders.dilId equals dil.id
                        join kur in db.Kur on ders.kurId equals kur.id
                        join ogrt in db.Ogretmen on ders.ogrtId equals ogrt.id
                        select new
                        {
                            ders.id,
                            ders.gun,
                            ders.saat,
                            dilad = dil.ad,
                            kur.seviye,
                            ogrtad = ogrt.ad + " " + ogrt.soyad,
                            ders.derslik

                        };
                return x.ToList();
            }
        }

        private void BtnProgEkle_Click(object sender, RoutedEventArgs e)
        {
            var x = Doldur(secilenId);
            GrdProg.Items.Add(x);
            secilenler.Add(secilenId);
            BtnKaydet.IsEnabled = true ;

        }
        List<int> secilenler = new List<int>();
        int secilenId, silinecekId;
        private void GrddersGoster_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            secilenId = Convert.ToInt32(((TextBlock)GrddersGoster.Columns[0].GetCellContent(GrddersGoster.SelectedItem)).Text);
            
        }

        private void BtnKaydet_Click(object sender, RoutedEventArgs e)
        {
            DersProg dp = new DersProg();
            dp.ad = txt_progAd.Text;
            using(DbDilKursuOtomsyonuEntities db=new DbDilKursuOtomsyonuEntities())
            {
                foreach (int i in secilenler)
                {
                    foreach(Ders d in db.Ders)
                    {
                        if (d.id == i)
                        {
                            dp.Ders.Add(d);
                            break;
                        }
                    }
                }
                db.DersProg.Add(dp);
                try
                {
                    MessageBox.Show("Kaydedildi.");
                    db.SaveChanges();
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Daha sonra tekrar deneyiniz." + ex.InnerException.Message);
                }
            }
            
        }

        private void GrdProg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //silinecekId = Convert.ToInt32(((TextBlock)GrdProg.Columns[0].GetCellContent(GrdProg.SelectedItem)).Text);
        }
        private void btn_yon_ayar_Click(object sender, RoutedEventArgs e)
        {
            using (DbDilKursuOtomsyonuEntities db = new DbDilKursuOtomsyonuEntities())
            {
                // Kullanıcı ayarları kısmında elemanın bilgilerini günceller
                var guncellenecek = (from v in db.Yonetici
                                     where v.kullaniciId == KId
                                     select v).FirstOrDefault();
                guncellenecek.email = txt_mail.Text;
                guncellenecek.tel = txt_tel.Text;
                guncellenecek.Kullanici.sifre = txt_sifre.Text;
                db.SaveChanges();
                MessageBox.Show("Güncellendi !");

            }
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (guncelle.IsSelected)
            {
                using (DbDilKursuOtomsyonuEntities db=new DbDilKursuOtomsyonuEntities())
                {
                    // ayarlar kısmını açtığımız anda ad ve soyadın karşımıza gelip değişiklik yapılamaması için
                    var sor = (from v in db.Yonetici
                               where v.kullaniciId == KId
                               select v).FirstOrDefault();
                    txt_ad.Text = sor.ad;
                    txt_soyad.Text = sor.soyad;

                    txt_ad.IsEnabled = false;
                    txt_soyad.IsEnabled = false;
                }
            }
        }

        
        private void txt_tel_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!char.IsDigit(e.Text, e.Text.Length - 1))
            {
                e.Handled = true;
            }
        }














    }
}
